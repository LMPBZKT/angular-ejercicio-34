import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-primer-price',
  templateUrl: './primer-price.component.html',
  styleUrls: ['./primer-price.component.css']
})
export class PrimerPriceComponent implements OnInit {

  constructor(private router: ActivatedRoute) { 
    this.router.params.subscribe(parametros => {

    });

    this.router.parent?.params.subscribe(parametros => {
      
    })
  }

  ngOnInit(): void {
  }

}
